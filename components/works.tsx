"use client";

import { useState, useRef, useEffect } from "react";
import { Transition } from "@headlessui/react";
import Image from "next/image";
import Community from "@/public/images/community.png";

import Waste from "@/public/images/waste.png";
import Trash from "@/public/images/trash.png";
import Trash1 from "@/public/images/trash1.png";
import Trash2 from "@/public/images/trash2.png";

import Transport from "@/public/images/transport.png";
import Pass from "@/public/images/pass.png";
import Metro from "@/public/images/metro.png";

import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader

export default function Works() {
  const [tab, setTab] = useState<number>(1);

  const tabs = useRef<HTMLDivElement>(null);

  const heightFix = () => {
    if (tabs.current && tabs.current.parentElement)
      tabs.current.parentElement.style.height = `${tabs.current.clientHeight}px`;
  };

  useEffect(() => {
    heightFix();
  }, []);

  function classNames(...classes: any[]) {
    return classes.filter(Boolean).join(" ");
  }

  return (
    <section className="relative" id="works">
      {/* Section background (needs .relative class on parent and next sibling elements) */}
      <div
        className="absolute inset-0 bg-gray-100 pointer-events-none "
        aria-hidden="true"
      ></div>
      <div className="absolute left-0 right-0 m-auto w-px p-px h-16 bg-gray-200 transform -translate-y-1/2"></div>

      <div className="relative max-w-6xl mx-auto px-4 sm:px-6">
        <div className="pt-10 md:pt-10">
          {/* Section header */}
          <div className="max-w-3xl mx-auto text-center pb-10 md:pb-10">
            <h1 className="h2 mb-4">How It Works</h1>
            <p className="text-xl text-gray-600">
              A user-friendly smartphone app that encourages eco-friendly habits
            </p>
          </div>

          {/* Section content */}
          <div className="md:grid md:grid-cols-12 md:gap-6">
            {/* Content */}
            <div
              className="max-w-xl md:max-w-none md:w-full mx-auto md:col-span-7 lg:col-span-6 md:mt-6"
              data-aos="fade-right"
            >
              <div className="md:pr-4 lg:pr-12 xl:pr-16 mb-8">
                <h3 className="h3 mb-3">
                  <span className="bg-clip-text text-transparent bg-gradient-to-r from-green-500 to-green-400">
                    GreenScore
                  </span>{" "}
                  is a mobile app that helps you track your green actions.
                </h3>
                <p className="text-xl text-gray-600">
                  Downloading the app and creating an account grants access to a
                  platform that tracks your green actions and offers enticing
                  rewards and incentives
                </p>
              </div>
              {/* Tabs buttons */}
              <div className="mb-8 md:mb-0">
                <a
                  className={`flex items-center text-lg p-5 rounded border transition duration-300 ease-in-out mb-3 ${
                    tab !== 1
                      ? "bg-white shadow-md border-gray-200 hover:shadow-lg"
                      : "bg-gray-200 border-transparent"
                  }`}
                  href="#0"
                  onClick={(e) => {
                    e.preventDefault();
                    setTab(1);
                  }}
                >
                  <div>
                    <div className="font-bold leading-snug tracking-tight mb-1">
                      Recycling: e-waste and home waste
                    </div>
                    <div className="text-gray-600">
                      Through the GreenScore app, you can log your recycling
                      efforts
                    </div>
                  </div>
                  <div className="flex justify-center items-center w-8 h-8 bg-white rounded-full shadow flex-shrink-0 ml-3">
                    <svg
                      className="w-3 h-3 fill-current"
                      viewBox="0 0 12 12"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M11.854.146a.5.5 0 00-.525-.116l-11 4a.5.5 0 00-.015.934l4.8 1.921 1.921 4.8A.5.5 0 007.5 12h.008a.5.5 0 00.462-.329l4-11a.5.5 0 00-.116-.525z"
                        fillRule="nonzero"
                      />
                    </svg>
                  </div>
                </a>
                <a
                  className={`flex items-center text-lg p-5 rounded border transition duration-300 ease-in-out mb-3 ${
                    tab !== 2
                      ? "bg-white shadow-md border-gray-200 hover:shadow-lg"
                      : "bg-gray-200 border-transparent"
                  }`}
                  href="#0"
                  onClick={(e) => {
                    e.preventDefault();
                    setTab(2);
                  }}
                >
                  <div>
                    <div className="font-bold leading-snug tracking-tight mb-1">
                      Public Transportation
                    </div>
                    <div className="text-gray-600">
                      With GreenScore, you can easily track your usage of public
                      transportation
                    </div>
                  </div>
                  <div className="flex justify-center items-center w-8 h-8 bg-white rounded-full shadow flex-shrink-0 ml-3">
                    <svg
                      className="w-3 h-3 fill-current"
                      viewBox="0 0 12 12"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M11.854.146a.5.5 0 00-.525-.116l-11 4a.5.5 0 00-.015.934l4.8 1.921 1.921 4.8A.5.5 0 007.5 12h.008a.5.5 0 00.462-.329l4-11a.5.5 0 00-.116-.525z"
                        fillRule="nonzero"
                      />
                    </svg>
                  </div>
                </a>
                <a
                  className={`flex items-center text-lg p-5 rounded border transition duration-300 ease-in-out mb-3 ${
                    tab !== 3
                      ? "bg-white shadow-md border-gray-200 hover:shadow-lg"
                      : "bg-gray-200 border-transparent"
                  }`}
                  href="#0"
                  onClick={(e) => {
                    e.preventDefault();
                    setTab(3);
                  }}
                >
                  <div>
                    <div className="font-bold leading-snug tracking-tight mb-1">
                      Community Engagement
                    </div>
                    <div className="text-gray-600">
                      GreenScore app provides a platform for individuals to
                      connect and develop their community
                    </div>
                  </div>
                  <div className="flex justify-center items-center w-8 h-8 bg-white rounded-full shadow flex-shrink-0 ml-3">
                    <svg
                      className="w-3 h-3 fill-current"
                      viewBox="0 0 12 12"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M11.854.146a.5.5 0 00-.525-.116l-11 4a.5.5 0 00-.015.934l4.8 1.921 1.921 4.8A.5.5 0 007.5 12h.008a.5.5 0 00.462-.329l4-11a.5.5 0 00-.116-.525z"
                        fillRule="nonzero"
                      />
                    </svg>
                  </div>
                </a>
              </div>
            </div>

            {/* Tabs items */}
            <div className="max-w-xl md:max-w-none md:w-full mx-auto md:col-span-5 lg:col-span-6 mb-8 md:mb-0 md:order-1">
              <div className="transition-all">
                <div
                  className="relative flex text-justify"
                  data-aos="zoom-y-out"
                  ref={tabs}
                >
                  {/* Item 1 */}
                  <Transition
                    show={tab === 1}
                    appear={true}
                    className="w-full"
                    enter="transition ease-in-out duration-700 transform order-first"
                    enterFrom="opacity-0 translate-y-16"
                    enterTo="opacity-100 translate-y-0"
                    leave="transition ease-in-out duration-300 transform absolute"
                    leaveFrom="opacity-100 translate-y-0"
                    leaveTo="opacity-0 -translate-y-16"
                    beforeEnter={() => heightFix()}
                    unmount={false}
                  >
                    <div className="relative">
                      <br />
                      <p className="text-xl text-gray-600">
                        We recognize the importance of recycling in reducing
                        waste and conserving valuable resources. Through the
                        GreenScore app, you can log your recycling efforts,
                        whether it's disposing e-waste or home waste. As you
                        accumulate points for your recycling achievements, you
                        gain rewards in the form of discounts, vouchers, and
                        coupons.
                      </p>

                      <div>
                        <Image
                          className="md:max-w-none mx-auto rounded"
                          src={Trash}
                          width="462"
                          height="462"
                          alt="Features bg"
                        />
                      </div>
                      {/* <Carousel
                        showThumbs={false}
                        showStatus={false}
                        showIndicators={false}
                        infiniteLoop={true}
                        autoPlay={true}
                        interval={5000}
                        transitionTime={500}
                        stopOnHover={false}
                        swipeable={true}
                        emulateTouch={true}
                        showArrows={false}
                        className="md:max-w-none mx-auto rounded"
                      >

                        <div>
                          <Image
                            className="md:max-w-none mx-auto rounded"
                            src={Trash1}
                            width={462}
                            height="462"
                            alt="Features bg"
                          />
                        </div>
                        <div>
                          <Image
                            className="md:max-w-none mx-auto rounded"
                            src={Trash2}
                            width={462}
                            height="462"
                            alt="Features bg"
                          />
                        </div>
                      </Carousel> */}
                    </div>
                  </Transition>
                  {/* Item 2 */}
                  <Transition
                    show={tab === 2}
                    appear={true}
                    className="w-full"
                    enter="transition ease-in-out duration-700 transform order-first"
                    enterFrom="opacity-0 translate-y-16"
                    enterTo="opacity-100 translate-y-0"
                    leave="transition ease-in-out duration-300 transform absolute"
                    leaveFrom="opacity-100 translate-y-0"
                    leaveTo="opacity-0 -translate-y-16"
                    beforeEnter={() => heightFix()}
                    unmount={false}
                  >
                    <div className="relative">
                      <br />
                      <p className="text-xl text-gray-600">
                        Choosing public transportation is a significant step
                        towards reducing carbon emissions and easing traffic
                        congestion. With GreenScore, you can easily track your
                        usage of public transportation, such as corporate buses,
                        metro-rails, subways and trains. As one accumulates
                        GreenScore points for each trip, one not only
                        contributes to cleaner air but also earns rewards that
                        make your sustainable choices even more rewarding.
                      </p>
                      <br />

                      <div className="relative inline-flex flex-col">
                        <Carousel
                          showThumbs={false}
                          showStatus={false}
                          showIndicators={false}
                          infiniteLoop={true}
                          autoPlay={true}
                          interval={5000}
                          transitionTime={500}
                          stopOnHover={false}
                          swipeable={true}
                          emulateTouch={true}
                          showArrows={false}
                          className="md:max-w-none mx-auto rounded"
                        >
                          <div>
                            <Image
                              className="md:max-w-none mx-auto rounded"
                              src={Transport}
                              width={500}
                              height="462"
                              alt="Features bg"
                            />
                          </div>
                          <div>
                            <Image
                              className="md:max-w-none mx-auto rounded"
                              src={Metro}
                              width={500}
                              height="462"
                              alt="Features bg"
                            />
                          </div>
                          <div>
                            <Image
                              className="md:max-w-none mx-auto rounded"
                              src={Pass}
                              width={500}
                              height="462"
                              alt="Features bg"
                            />
                          </div>
                        </Carousel>
                      </div>
                    </div>
                  </Transition>
                  {/* Item 3 */}
                  <Transition
                    show={tab === 3}
                    appear={true}
                    className="w-full"
                    enter="transition ease-in-out duration-700 transform order-first"
                    enterFrom="opacity-0 translate-y-16"
                    enterTo="opacity-100 translate-y-0"
                    leave="transition ease-in-out duration-300 transform absolute"
                    leaveFrom="opacity-100 translate-y-0"
                    leaveTo="opacity-0 -translate-y-16"
                    beforeEnter={() => heightFix()}
                    unmount={false}
                  >
                    <div className="relative">
                      <br />
                      <p className="text-xl text-gray-600">
                        We believe that collective action is crucial in driving
                        sustainable change. GreenScore app provides a platform
                        for individuals to connect and develop their community
                        through eco-friendly actions, share their green
                        achievements, and inspire others within our thriving
                        community. Users can engage in friendly competitions,
                        join challenges, and celebrate milestones together.
                        Together, we can create a ripple effect that inspires
                        more people to embrace sustainable living.
                      </p>
                      <br />
                      <div className="relative inline-flex flex-col">
                        <Image
                          className="md:max-w-none mx-auto rounded"
                          src={Community}
                          width={500}
                          height="462"
                          alt="Features bg"
                        />
                      </div>
                    </div>
                  </Transition>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
