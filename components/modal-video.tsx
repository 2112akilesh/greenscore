"use client";

import { useState, useRef, Fragment } from "react";
import type { StaticImageData } from "next/image";
import { Disclosure, Dialog, Transition } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/20/solid";
import Image from "next/image";
import Wave from "react-wavify";

interface ModalVideoProps {
  thumb: StaticImageData;
  thumbWidth: number;
  thumbHeight: number;
  thumbAlt: string;
}

export default function ModalVideo({
  thumb,
  thumbWidth,
  thumbHeight,
  thumbAlt,
}: ModalVideoProps) {
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const videoRef = useRef<HTMLVideoElement>(null);

  return (
    <section id="about">
      <Wave
        fill="url(#gradient)"
        className="absolute bottom-0 left-0 right-0 top-1/3 -z-10"
        paused={false}
        options={{
          height: 20,
          amplitude: 40,
          speed: 0.15,
          points: 3,
        }}
      >
        <defs>
          <linearGradient id="gradient" gradientTransform="rotate(90)">
            <stop offset="5%" stopColor="#bbf7d0" />
            <stop offset="90%" stopColor="#f0fdf4" />
          </linearGradient>
        </defs>
      </Wave>

      <div className="relative max-w-6xl mx-auto px-4 sm:px-6">
        <div className="pt-12 md:pt-20">
          {/* Section header */}
          <div className="max-w-5xl mx-auto text-center pb-12 md:pb-16">
            {/* <div className="flex flex-col justify-center">
              <Image
                className="absolute left-1/2 transform -translate-x-1/2 bottom-0 pointer-events-none -mb-30"
                src={thumb}
                width={500}
                height={thumbHeight}
                alt={thumbAlt}
              /> 
              </div> */}

            {/* center the image */}
            <Image
              className="mx-auto"
              src={thumb}
              width={500}
              height={thumbHeight}
              alt={thumbAlt}
            />
            <h2 className="h2 mb-4">About Us</h2>
            <p
              className="text-xl text-green-700 text-justify md:text-center"
              data-aos="zoom-y-out"
              data-aos-delay="150"
            >
              At 2050 Technologies Inc., we are committed to fostering a greener
              and more sustainable future. At the core of our belief is the idea
              that individual actions can make a big difference in protecting
              our planet. To empower and motivate people, we have developed an
              innovative mobile application that allows users to
            </p>

            <div className="max-w-3xl mx-auto " data-aos="zoom-y-out">
              <div className="text-center px-0 mx-0 sm:px-12 sm:mx-4 py-8 pt-12  md:mx-0">
                <Disclosure as="div" className="mt-0 sm:mt-8">
                  {({ open }) => (
                    <>
                      <Disclosure.Button className="flex w-full justify-between rounded-lg bg-green-300 px-4 py-2 text-left text-sm font-medium text-green-900 hover:bg-green-300 focus:outline-none focus-visible:ring focus-visible:ring-green-500 focus-visible:ring-opacity-75">
                        <span>Make green choices</span>
                        <ChevronUpIcon
                          className={`${
                            open ? "transform rotate-180" : ""
                          } w-5 h-5 text-green-500`}
                        />
                      </Disclosure.Button>
                      <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-green-900 bg-green-200 text-left">
                        based on users' T3R (travel, reduce reuse & recycle)
                        choices.
                      </Disclosure.Panel>
                    </>
                  )}
                </Disclosure>
                <Disclosure as="div" className="mt-2">
                  {({ open }) => (
                    <>
                      <Disclosure.Button className="flex w-full justify-between rounded-lg bg-green-300 px-4 py-2 text-left text-sm font-medium text-green-900 hover:bg-green-300 focus:outline-none focus-visible:ring focus-visible:ring-green-500 focus-visible:ring-opacity-75">
                        <span>Calculate personal carbon footprint</span>
                        <ChevronUpIcon
                          className={`${
                            open ? "transform rotate-180" : ""
                          } w-5 h-5 text-green-500`}
                        />
                      </Disclosure.Button>
                      <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-green-900 bg-green-200 text-left">
                        By providing GreenScore points based on their T3R
                        choices
                      </Disclosure.Panel>
                    </>
                  )}
                </Disclosure>
                <Disclosure as="div" className="mt-2">
                  {({ open }) => (
                    <>
                      <Disclosure.Button className="flex w-full justify-between rounded-lg bg-green-300 px-4 py-2 text-left text-sm font-medium text-green-900 hover:bg-green-300 focus:outline-none focus-visible:ring focus-visible:ring-green-500 focus-visible:ring-opacity-75">
                        <span>Get rewarded for eco-friendly actions</span>
                        <ChevronUpIcon
                          className={`${
                            open ? "transform rotate-180" : ""
                          } w-5 h-5 text-green-500`}
                        />
                      </Disclosure.Button>
                      <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-green-900 bg-green-200 text-left">
                        By enabling them to redeem their GreenScore points
                      </Disclosure.Panel>
                    </>
                  )}
                </Disclosure>
              </div>
            </div>
            <p
              className="text-xl text-green-900 mt-8 sm:block hidden "
              data-aos="zoom-y-out"
              data-aos-delay="150"
            >
              By harnessing the power of social technology and leveraging the
              collective strength of individuals, we aim to inspire and empower
              everyone to take small but impactful actions towards building a
              more sustainable future.
            </p>
          </div>
          {/* <div className="mb-8 relative border-2 border-green-400 rounded bg-green-200 p-8"> */}

          <h3 className="h3 flex justify-center">Founder: Bharat Avasarala</h3>
          <p
            className="text-md text-green-900 mt-2 text-justify "
            data-aos="zoom-y-out"
            data-aos-delay="150"
          >
            Bharat Avasarala is a seasoned engineering manager with experience
            in R&D, technology & product management at leading tech companies
            including Apple and Google. He has a Ph.D in Materials Sci & Engg
            focussing on Clean Energy Technologies (Hydrogen Fuel cells and
            Batteries). As a CEO of 2050 Technologies, Bharat aims to bring his
            scientific and technical expertise to positively impact the
            environment.
          </p>
          {/* </div> */}
          <br />
          {/* <div className="mb-8 relative border-2 border-green-400 rounded bg-green-100 p-8"> */}
          <h3 className="h3 flex justify-center ">Co-founder: Shayoni Ray</h3>
          <p
            className="text-md text-green-900 mt-2 text-justify"
            data-aos="zoom-y-out"
            data-aos-delay="150"
          >
            Shayoni Ray, PhD is an accomplished molecular scientist with
            experience in Computational Modeling and Drug Discovery at NASA and
            several leading Biopharma companies. As a co-founder, Shayoni is
            driven by her commitment to developing solutions that improve
            quality of life and sustainability.
          </p>
        </div>
        {/* </div> */}

        {/* End: Video thumbnail */}
      </div>
    </section>
  );
}
