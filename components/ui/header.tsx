"use client";

import { useState, useEffect } from "react";

import Link from "next/link";
import Logo from "./logo";
import Dropdown from "@/components/utils/dropdown";
import MobileMenu from "./mobile-menu";
import Image from "next/image";
import Leaf from "@/public/images/leaf.png";

export default function Header() {
  const [top, setTop] = useState<boolean>(true);

  // detect whether user has scrolled the page down by 10px
  const scrollHandler = () => {
    window.pageYOffset > 10 ? setTop(false) : setTop(true);
  };

  const navbarLinks = [
    { label: "About Us", href: "#about", ariaLabel: "About Us" },
    { label: "Our Vision", href: "#vision", ariaLabel: "Our Vision" },
    { label: "How It Works", href: "#works", ariaLabel: "How It Works" },
    { label: "Partnerships", href: "#partnerships", ariaLabel: "Partnerships" },
    { label: "Join Community", href: "#join", ariaLabel: "Join Community" },
  ];

  useEffect(() => {
    scrollHandler();
    window.addEventListener("scroll", scrollHandler);
    return () => window.removeEventListener("scroll", scrollHandler);
  }, [top]);

  return (
    <header
      className={`fixed w-full z-30 md:bg-opacity-90 transition duration-300 ease-in-out ${
        !top ? "bg-white backdrop-blur-sm shadow-lg" : ""
      }`}
    >
      <div className="max-w-6xl mx-auto px-5 sm:px-6">
        <div className="flex items-center justify-between h-16 md:h-20">
          {/* Site branding */}
          <div className="shrink-0 mr-4">
            {/* <Logo /> */}
            <Image
              src={Leaf} alt="natural-food"
              width={48}
              height={48}
            />
          </div>

          <div>
            <span className="text-2xl sm:text-3xl font-bold font-se bg-clip-text text-transparent bg-gradient-to-r from-green-600 to-green-400 ">
              <a href="/">2050 Technologies</a>
            </span>
          </div>

          {/* Desktop navigation */}
          <nav className="hidden md:flex md:grow">
            {/* Desktop sign in links */}
            <ul className="flex grow justify-end flex-wrap items-center">
              {/* <li>               
                <Link
                  href="/signin"
                  className="font-medium text-gray-600 hover:text-gray-900 px-5 py-3 flex items-center transition duration-150 ease-in-out"
                >
                  Sign in
                </Link>
              </li> */}
              {navbarLinks.map(({ href, label, ariaLabel }) => (
                <li className="hidden lg:flex" key={label}>
                  <a
                    className="font-medium text-gray-600 hover:text-gray-900 px-5 py-3 flex items-center transition duration-150 ease-in-out"
                    href={href}
                    aria-label={ariaLabel}
                  >
                    <span>{label}</span>
                  </a>
                </li>
              ))}

              {/* <li>
                <Link
                  href="#join"
                  className="btn-sm text-gray-200 bg-gray-900 hover:bg-gray-800 ml-3"
                >
                  <span>Join Community</span>
                  <svg
                    className="w-3 h-3 fill-current text-gray-400 shrink-0 ml-2 -mr-1"
                    viewBox="0 0 12 12"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M11.707 5.293L7 .586 5.586 2l3 3H0v2h8.586l-3 3L7 11.414l4.707-4.707a1 1 0 000-1.414z"
                      fillRule="nonzero"
                    />
                  </svg>
                </Link>
              </li> */}
            </ul>
          </nav>

          <MobileMenu />
        </div>
      </div>
    </header>
  );
}
