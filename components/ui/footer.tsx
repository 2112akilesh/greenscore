import Logo from "./logo";
import Image from "next/image";
import Leaf from "@/public/images/leaf.png";

export default function Footer() {
  return (
    <footer>
      <div className="max-w-6xl mx-auto px-4 sm:px-6">
        {/* Top area: Blocks */}
        <div className="grid sm:grid-cols-2 gap-4 py-8 md:py-12 border-t border-gray-200">
          {/* 1st block */}
          <div className="">
            <div className="mb-2">
              <Image
                src={Leaf}
                alt="natural-food"
                width={32}
                height={32}
              />

            </div>
            <div className="text-sm text-gray-600">
              <a
                href="#0"
                className="text-gray-600 hover:text-gray-900 hover:underline transition duration-150 ease-in-out"
              >
                Terms
              </a>{" "}
              ·{" "}
              <a
                href="#0"
                className="text-gray-600 hover:text-gray-900 hover:underline transition duration-150 ease-in-out"
              >
                Privacy Policy
              </a>
            </div>
          </div>

          {/* Social as */}
          {/* <div className="">
            <ul className="flex mb-4 md:order-1 md:ml-4 md:mb-0">
              <li className="ml-4">
                <a
                  href="#0"
                  className="flex justify-center items-center text-gray-600 hover:text-gray-900 bg-white hover:bg-white-100 rounded-full shadow transition duration-150 ease-in-out"
                  aria-label="Twitter"
                >
                  <svg
                    className="w-8 h-8 fill-current"
                    viewBox="0 0 32 32"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M24 11.5c-.6.3-1.2.4-1.9.5.7-.4 1.2-1 1.4-1.8-.6.4-1.3.6-2.1.8-.6-.6-1.5-1-2.4-1-1.7 0-3.2 1.5-3.2 3.3 0 .3 0 .5.1.7-2.7-.1-5.2-1.4-6.8-3.4-.3.5-.4 1-.4 1.7 0 1.1.6 2.1 1.5 2.7-.5 0-1-.2-1.5-.4 0 1.6 1.1 2.9 2.6 3.2-.3.1-.6.1-.9.1-.2 0-.4 0-.6-.1.4 1.3 1.6 2.3 3.1 2.3-1.1.9-2.5 1.4-4.1 1.4H8c1.5.9 3.2 1.5 5 1.5 6 0 9.3-5 9.3-9.3v-.4c.7-.5 1.3-1.1 1.7-1.8z" />
                  </svg>
                </a>
              </li>
              <li className="ml-4">
                <a
                  href="#0"
                  className="flex justify-center items-center text-gray-600 hover:text-gray-900 bg-white hover:bg-white-100 rounded-full shadow transition duration-150 ease-in-out"
                  aria-label="Facebook"
                >
                  <svg
                    className="w-8 h-8 fill-current"
                    viewBox="0 0 32 32"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M14.023 24L14 17h-3v-3h3v-2c0-2.7 1.672-4 4.08-4 1.153 0 2.144.086 2.433.124v2.821h-1.67c-1.31 0-1.563.623-1.563 1.536V14H21l-1 3h-2.72v7h-3.257z" />
                  </svg>
                </a>
              </li>
            </ul>
          </div> */}

          {/* 3rd block */}
          <div className="">
            <h6 className="text-gray-800 font-medium mb-2">
              💬 Support and Feedback
            </h6>
            <p className="text-sm text-gray-600 ">
              For any queries/feedback contact:&nbsp;
              <a
                //href="https://2050-technologies.com"
                href="mailto:bharata@2050-technologies.com"
                target="_blank"
                className="text-sm text-gray-600 hover:text-gray-900 transition duration-150 ease-in-out"
              >
                bharata@2050-technologies.com
              </a>
            </p>
          </div>
        </div>

        {/* Bottom area */}
        <div className="md:flex md:items-center md:justify-between py-4 md:py-8 border-t border-green-200">
          {/* Copyrights note */}
          <div className="text-sm text-gray-600 mr-4 text-center">
            &copy; 2050-technologies.com. All rights reserved.
          </div>

          {/* Site By */}
          <div className="text-sm text-gray-600 mr-4 pt-3 md:pt-0 text-center">
            Site by{" "}
            <a
              href="https://www.akilesh.io"
              target="_blank"
              className="text-green-600 hover:text-gray-900 transition duration-150 ease-in-out"
            >
              akilesh_io
            </a>          
        </div>
        </div>
      </div>
    </footer>
  );
}
