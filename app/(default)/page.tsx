export const metadata = {
  title: 'GreenScore',
  description: 'Mobile app to help you track your carbon footprint and reduce it.',
}

import Hero from '@/components/hero'
import Vision from '@/components/vision'
import Works from '@/components/works'
import Partnerships from '@/components/partnerships'
import Join from '@/components/join'
import Newsletter from '@/components/newsletter'

export default function Home() {
  return (
    <>
      <Hero />
      <Vision />
      <Works />
      <Partnerships />
      <Join />
      <Newsletter />
    </>
  )
}
